package com.gitlab.qolq.dik;

import com.gitlab.qolq.dik.network.Networking;
import net.fabricmc.api.ModInitializer;
import net.fabricmc.fabric.api.networking.v1.ServerPlayNetworking;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public final class DIK implements ModInitializer
{
    public static final String MOD_ID = "dik";

    private static final Logger log = LogManager.getLogger(MOD_ID);

    private static boolean enabled = false;

    @Override
    public void onInitialize()
    {
        ServerPlayNetworking.registerGlobalReceiver(
            Networking.MOD_CHANNEL_ID,
            (server, player, handler, buf, responder)->Networking.receiveFromClient(server, handler, buf, player)
        );
    }

    public static boolean isEnabled()
    {
        return enabled;
    }

    public static void setEnabled(boolean bool)
    {
        log.debug(bool ? "Enabling DIK" : "Disabling DIK");
        enabled = bool;
    }
}
