package com.gitlab.qolq.dik.network;

import net.minecraft.entity.Entity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.network.PacketByteBuf;
import net.minecraft.network.listener.PacketListener;
import net.minecraft.util.Hand;
import net.minecraft.util.math.Vec3d;

public final class UseEntityAtC2SPacket extends UseEntityC2SPacket
{
    private Vec3d position;

    UseEntityAtC2SPacket() {}

    public UseEntityAtC2SPacket(Entity entity, Hand hand, boolean playerSneaking, Vec3d position)
    {
        super(entity, hand, playerSneaking);
        this.position = position;
    }

    @Override
    public void read(PacketByteBuf buf)
    {
        super.read(buf);
        position = new Vec3d(buf.readFloat(), buf.readFloat(), buf.readFloat());
    }

    @Override
    public void write(PacketByteBuf buf)
    {
        super.write(buf);
        buf.writeFloat((float)position.getX());
        buf.writeFloat((float)position.getY());
        buf.writeFloat((float)position.getZ());
    }

    @Override
    public void apply(PlayerEntity player, PacketListener listener)
    {
        ((DIKPacketListener)listener).dik$useEntityAt(
            player.getEntityWorld().getEntityById(entityId), hand, playerSneaking, position
        );
    }
}
