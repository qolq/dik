package com.gitlab.qolq.dik.network;

import java.util.function.Supplier;

import com.gitlab.qolq.dik.DIK;
import it.unimi.dsi.fastutil.bytes.Byte2ObjectMap;
import it.unimi.dsi.fastutil.bytes.Byte2ObjectOpenHashMap;
import it.unimi.dsi.fastutil.objects.Object2ByteMap;
import it.unimi.dsi.fastutil.objects.Object2ByteOpenHashMap;
import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;
import net.fabricmc.fabric.api.client.networking.v1.ClientPlayNetworking;
import net.fabricmc.fabric.api.networking.v1.PacketByteBufs;
import net.fabricmc.fabric.api.networking.v1.ServerPlayNetworking;
import net.minecraft.client.MinecraftClient;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.network.PacketByteBuf;
import net.minecraft.network.listener.PacketListener;
import net.minecraft.server.MinecraftServer;
import net.minecraft.server.network.ServerPlayerEntity;
import net.minecraft.util.Identifier;

public final class Networking
{
    public static final Identifier MOD_CHANNEL_ID = new Identifier(DIK.MOD_ID, "play");

    private static final Object2ByteMap<Class<? extends DIKPacket>> classToIdMap = new Object2ByteOpenHashMap<>();
    private static final Byte2ObjectMap<Supplier<? extends DIKPacket>> idToFactoryMap = new Byte2ObjectOpenHashMap<>();

    static
    {
        byte id = -1;
        registerPacket(++id, EnableModS2CPacket.class, EnableModS2CPacket::new);
        registerPacket(++id, UseBlockC2SPacket.class, UseBlockC2SPacket::new);
        registerPacket(++id, UseEntityAtC2SPacket.class, UseEntityAtC2SPacket::new);
        registerPacket(++id, UseEntityC2SPacket.class, UseEntityC2SPacket::new);
        registerPacket(++id, UseItemOnBlockC2SPacket.class, UseItemOnBlockC2SPacket::new);
        registerPacket(++id, UseItemOnEntityC2SPacket.class, UseItemOnEntityC2SPacket::new);
    }

    private static <T extends DIKPacket> void registerPacket(byte id, Class<T> clazz, Supplier<T> factory)
    {
        if (classToIdMap.containsKey(clazz))
            throw new IllegalArgumentException("Packet type already registered");
        classToIdMap.put(clazz, id);
        idToFactoryMap.put(id, factory);
    }

    @Environment(EnvType.CLIENT)
    public static void sendToServer(DIKPacket packet)
    {
        ClientPlayNetworking.send(MOD_CHANNEL_ID, byteBufFromPacket(packet));
    }

    public static void sendToClient(DIKPacket packet, ServerPlayerEntity player)
    {
        ServerPlayNetworking.send(player, MOD_CHANNEL_ID, byteBufFromPacket(packet));
    }

    @Environment(EnvType.CLIENT)
    public static void receiveFromServer(MinecraftClient client, PacketListener listener, PacketByteBuf buf)
    {
        DIKPacket packet = packetFromByteBuf(buf);
        // make sure the lambda doesn't have parameters that are client-only. fixes dedicated server crash
        PlayerEntity player = client.player;
        client.execute(()->packet.apply(player, listener));
    }

    public static void receiveFromClient(MinecraftServer server, PacketListener listener, PacketByteBuf buf,
                                         ServerPlayerEntity player)
    {
        DIKPacket packet = packetFromByteBuf(buf);
        server.execute(()->packet.apply(player, listener));
    }

    private static PacketByteBuf byteBufFromPacket(DIKPacket packet)
    {
        if (!classToIdMap.containsKey(packet.getClass()))
            throw new IllegalArgumentException("Unregistered packet type: " + packet.getClass().getName());

        PacketByteBuf buf = PacketByteBufs.create();
        buf.writeByte(classToIdMap.getByte(packet.getClass()));
        packet.write(buf);

        return buf;
    }

    private static DIKPacket packetFromByteBuf(PacketByteBuf buf)
    {
        byte id = buf.readByte();

        if (!idToFactoryMap.containsKey(id))
            throw new IllegalArgumentException("Unregistered packet id: " + id);

        DIKPacket packet = idToFactoryMap.get(id).get();
        packet.read(buf);

        return packet;
    }
}
