package com.gitlab.qolq.dik.network;

import com.gitlab.qolq.dik.DIK;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.network.PacketByteBuf;
import net.minecraft.network.listener.PacketListener;

public final class EnableModS2CPacket implements DIKPacket
{
    @Override
    public void read(PacketByteBuf buf) {}

    @Override
    public void write(PacketByteBuf buf) {}

    @Override
    public void apply(PlayerEntity player, PacketListener listener)
    {
        DIK.setEnabled(true);
    }
}
