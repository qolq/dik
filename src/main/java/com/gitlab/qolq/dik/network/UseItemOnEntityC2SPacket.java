package com.gitlab.qolq.dik.network;

import net.minecraft.entity.Entity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.network.PacketByteBuf;
import net.minecraft.network.listener.PacketListener;
import net.minecraft.util.Hand;

public final class UseItemOnEntityC2SPacket implements DIKPacket
{
    private int entityId;
    private Hand hand;
    private boolean playerSneaking;

    UseItemOnEntityC2SPacket() {}

    public UseItemOnEntityC2SPacket(Entity entity, Hand hand, boolean playerSneaking)
    {
        entityId = entity.getId();
        this.hand = hand;
        this.playerSneaking = playerSneaking;
    }

    @Override
    public void read(PacketByteBuf buf)
    {
        entityId = buf.readVarInt();
        hand = buf.readEnumConstant(Hand.class);
        playerSneaking = buf.readBoolean();
    }

    @Override
    public void write(PacketByteBuf buf)
    {
        buf.writeVarInt(entityId);
        buf.writeEnumConstant(hand);
        buf.writeBoolean(playerSneaking);
    }

    @Override
    public void apply(PlayerEntity player, PacketListener listener)
    {
        ((DIKPacketListener)listener).dik$useItemOnEntity(
            player.getEntityWorld().getEntityById(entityId), hand, playerSneaking
        );
    }
}
