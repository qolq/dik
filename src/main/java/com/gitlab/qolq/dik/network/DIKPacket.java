package com.gitlab.qolq.dik.network;

import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.network.PacketByteBuf;
import net.minecraft.network.listener.PacketListener;

interface DIKPacket
{
    void read(PacketByteBuf buf);

    void write(PacketByteBuf buf);

    void apply(PlayerEntity player, PacketListener listener);
}
