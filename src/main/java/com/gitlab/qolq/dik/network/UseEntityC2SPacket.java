package com.gitlab.qolq.dik.network;

import net.minecraft.entity.Entity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.network.PacketByteBuf;
import net.minecraft.network.listener.PacketListener;
import net.minecraft.util.Hand;

public class UseEntityC2SPacket implements DIKPacket
{
    protected int entityId;
    protected Hand hand;
    protected boolean playerSneaking;

    UseEntityC2SPacket() {}

    public UseEntityC2SPacket(Entity entity, Hand hand, boolean playerSneaking)
    {
        entityId = entity.getId();
        this.hand = hand;
        this.playerSneaking = playerSneaking;
    }

    @Override
    public void read(PacketByteBuf buf)
    {
        entityId = buf.readVarInt();
        hand = buf.readEnumConstant(Hand.class);
        playerSneaking = buf.readBoolean();
    }

    @Override
    public void write(PacketByteBuf buf)
    {
        buf.writeVarInt(entityId);
        buf.writeEnumConstant(hand);
        buf.writeBoolean(playerSneaking);
    }

    @Override
    public void apply(PlayerEntity player, PacketListener listener)
    {
        ((DIKPacketListener)listener).dik$useEntity(
            player.getEntityWorld().getEntityById(entityId), hand, playerSneaking
        );
    }
}
