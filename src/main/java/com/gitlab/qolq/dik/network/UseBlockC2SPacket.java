package com.gitlab.qolq.dik.network;

import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.network.PacketByteBuf;
import net.minecraft.network.listener.PacketListener;
import net.minecraft.util.Hand;
import net.minecraft.util.hit.BlockHitResult;

public final class UseBlockC2SPacket implements DIKPacket
{
    private BlockHitResult target;
    private Hand hand;

    UseBlockC2SPacket() {}

    public UseBlockC2SPacket(BlockHitResult target, Hand hand)
    {
        this.target = target;
        this.hand = hand;
    }

    @Override
    public void read(PacketByteBuf buf)
    {
        target = buf.readBlockHitResult();
        hand = buf.readEnumConstant(Hand.class);
    }

    @Override
    public void write(PacketByteBuf buf)
    {
        buf.writeBlockHitResult(target);
        buf.writeEnumConstant(hand);
    }

    @Override
    public void apply(PlayerEntity player, PacketListener listener)
    {
        ((DIKPacketListener)listener).dik$useBlock(target, hand);
    }
}
