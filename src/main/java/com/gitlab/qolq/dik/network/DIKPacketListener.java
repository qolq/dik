package com.gitlab.qolq.dik.network;

import net.minecraft.entity.Entity;
import net.minecraft.util.Hand;
import net.minecraft.util.hit.BlockHitResult;
import net.minecraft.util.math.Vec3d;

public interface DIKPacketListener
{
    void dik$useBlock(BlockHitResult target, Hand hand);

    void dik$useItemOnBlock(BlockHitResult target, Hand hand);

    void dik$useEntityAt(Entity target, Hand hand, boolean playerSneaking, Vec3d position);

    void dik$useEntity(Entity target, Hand hand, boolean playerSneaking);

    void dik$useItemOnEntity(Entity target, Hand hand, boolean playerSneaking);
}
