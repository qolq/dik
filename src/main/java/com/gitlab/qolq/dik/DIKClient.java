package com.gitlab.qolq.dik;

import com.gitlab.qolq.dik.network.Networking;
import net.fabricmc.api.ClientModInitializer;
import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;
import net.fabricmc.fabric.api.client.keybinding.v1.KeyBindingHelper;
import net.fabricmc.fabric.api.client.networking.v1.ClientPlayConnectionEvents;
import net.fabricmc.fabric.api.client.networking.v1.ClientPlayNetworking;
import net.minecraft.client.option.KeyBinding;
import org.lwjgl.glfw.GLFW;

@Environment(EnvType.CLIENT)
public final class DIKClient implements ClientModInitializer
{
    private static KeyBinding interactionKey;

    @Override
    public void onInitializeClient()
    {
        interactionKey = KeyBindingHelper.registerKeyBinding(
            new KeyBinding(DIK.MOD_ID + ".key.interact", GLFW.GLFW_KEY_R, "key.categories.gameplay")
        );
        ClientPlayNetworking.registerGlobalReceiver(
            Networking.MOD_CHANNEL_ID,
            (client, handler, buf, responder)->Networking.receiveFromServer(client, handler, buf)
        );
        ClientPlayConnectionEvents.DISCONNECT.register((handler, client)->DIK.setEnabled(false));
    }

    public static KeyBinding getInteractionKey()
    {
        return interactionKey;
    }
}
