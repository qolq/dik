package com.gitlab.qolq.dik.mixin.client;

import com.gitlab.qolq.dik.DIK;
import com.gitlab.qolq.dik.DIKClient;
import com.gitlab.qolq.dik.DIKInteractionManager;
import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;
import net.minecraft.client.MinecraftClient;
import net.minecraft.client.network.ClientPlayerEntity;
import net.minecraft.client.network.ClientPlayerInteractionManager;
import net.minecraft.client.option.KeyBinding;
import net.minecraft.client.render.GameRenderer;
import net.minecraft.client.world.ClientWorld;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ActionResult;
import net.minecraft.util.Hand;
import net.minecraft.util.hit.BlockHitResult;
import net.minecraft.util.hit.EntityHitResult;
import net.minecraft.util.hit.HitResult;
import net.minecraft.util.math.Vec3d;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.Unique;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.Redirect;
import org.spongepowered.asm.mixin.injection.Slice;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

@Environment(EnvType.CLIENT)
@Mixin(MinecraftClient.class)
@SuppressWarnings("unused")
public abstract class MinecraftClientMixin
{
    @Shadow
    public GameRenderer gameRenderer;

    @Shadow
    public ClientPlayerInteractionManager interactionManager;

    @Shadow
    public ClientWorld world;

    @Shadow
    public ClientPlayerEntity player;

    @Shadow
    public HitResult crosshairTarget;

    @Shadow
    private int itemUseCooldown;

    @Shadow
    protected abstract void doItemUse();

    @Inject(
        method = "handleInputEvents()V",
        slice = @Slice(
            from = @At(value = "FIELD", target = "Lnet/minecraft/client/MinecraftClient;itemUseCooldown:I"),
            to = @At(value = "INVOKE", target = "Lnet/minecraft/client/MinecraftClient;handleBlockBreaking(Z)V")
        ),
        at = @At(
            value = "INVOKE",
            target = "Lnet/minecraft/client/MinecraftClient;doItemUse()V",
            ordinal = 0,
            shift = At.Shift.BY,
            by = 2
        )
    )
    private void doInteraction(CallbackInfo info)
    {
        if (!player.isUsingItem())
        {
            KeyBinding key = DIKClient.getInteractionKey();

            while (key.wasPressed())
                interact();

            if (itemUseCooldown == 0 && key.isPressed())
                interact();
        }
    }

    @Unique
    private void interact()
    {
        if (!DIK.isEnabled())
        {
            doItemUse();
            return;
        }

        if (interactionManager.isBreakingBlock())
            return;

        itemUseCooldown = 4;

        if (crosshairTarget == null || player.isRiding())
            return;

        switch (crosshairTarget.getType())
        {
            case BLOCK -> useBlock((BlockHitResult)crosshairTarget);
            case ENTITY -> useEntity((EntityHitResult)crosshairTarget);
        }
    }

    @Unique
    private void useBlock(BlockHitResult target)
    {
        for (Hand hand : Hand.values())
        {
            ItemStack stack = player.getStackInHand(hand);
            int i = stack.getCount();
            ActionResult result = ((DIKInteractionManager)interactionManager).dik$useBlock(target, world, player, hand);

            if (result.isAccepted())
            {
                if (result.shouldSwingHand())
                {
                    player.swingHand(hand);
                    if (!stack.isEmpty() && (stack.getCount() != i || interactionManager.hasCreativeInventory()))
                        gameRenderer.firstPersonRenderer.resetEquipProgress(hand);
                }
                break;
            }
            else if (result == ActionResult.FAIL)
            {
                break;
            }
        }
    }

    @Redirect(
        method = "doItemUse()V",
        at = @At(
            value = "INVOKE",
            target = "Lnet/minecraft/client/network/ClientPlayerInteractionManager;" +
                     "interactBlock(Lnet/minecraft/client/network/ClientPlayerEntity;" +
                     "Lnet/minecraft/client/world/ClientWorld;Lnet/minecraft/util/Hand;" +
                     "Lnet/minecraft/util/hit/BlockHitResult;)Lnet/minecraft/util/ActionResult;",
            ordinal = 0
        )
    )
    private ActionResult useItemOnBlock(ClientPlayerInteractionManager interactionManager, ClientPlayerEntity player,
                                        ClientWorld world, Hand hand, BlockHitResult target)
    {
        return DIK.isEnabled() ?
               ((DIKInteractionManager)interactionManager).dik$useItemOnBlock(target, world, player, hand) :
               interactionManager.interactBlock(player, world, hand, target);
    }

    @Unique
    private void useEntity(EntityHitResult target)
    {
        for (Hand hand : Hand.values())
        {
            DIKInteractionManager manager = ((DIKInteractionManager)interactionManager);
            Entity entity = target.getEntity();
            Vec3d position = target.getPos().subtract(entity.getX(), entity.getY(), entity.getZ());

            ActionResult result = manager.dik$useEntityAt(entity, player, hand, position);
            if (!result.isAccepted())
                result = manager.dik$useEntity(entity, player, hand);

            if (result.isAccepted())
            {
                if (result.shouldSwingHand())
                    player.swingHand(hand);
                break;
            }
        }
    }

    @Redirect(
        method = "doItemUse()V",
        at = @At(
            value = "INVOKE",
            target = "Lnet/minecraft/client/network/ClientPlayerInteractionManager;" +
                     "interactEntityAtLocation(Lnet/minecraft/entity/player/PlayerEntity;" +
                     "Lnet/minecraft/entity/Entity;Lnet/minecraft/util/hit/EntityHitResult;Lnet/minecraft/util/Hand;)" +
                     "Lnet/minecraft/util/ActionResult;",
            ordinal = 0
        )
    )
    private ActionResult cancelUseEntity(ClientPlayerInteractionManager interactionManager, PlayerEntity player,
                                         Entity target, EntityHitResult hitResult, Hand hand)
    {
        return DIK.isEnabled() ? ActionResult.PASS :
               interactionManager.interactEntityAtLocation(player, target, hitResult, hand);
    }

    @Redirect(
        method = "doItemUse()V",
        at = @At(
            value = "INVOKE",
            target = "Lnet/minecraft/client/network/ClientPlayerInteractionManager;interactEntity " +
                     "(Lnet/minecraft/entity/player/PlayerEntity;Lnet/minecraft/entity/Entity;" +
                     "Lnet/minecraft/util/Hand;)Lnet/minecraft/util/ActionResult;",
            ordinal = 0
        )
    )
    private ActionResult useItemOnEntity(ClientPlayerInteractionManager interactionManager, PlayerEntity player,
                                         Entity target, Hand hand)
    {
        return DIK.isEnabled() ?
               ((DIKInteractionManager)interactionManager).dik$useItemOnEntity(target, player, hand) :
               interactionManager.interactEntity(player, target, hand);
    }
}
