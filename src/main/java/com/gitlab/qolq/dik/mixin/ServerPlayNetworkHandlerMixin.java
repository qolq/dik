package com.gitlab.qolq.dik.mixin;

import java.util.function.Supplier;

import com.gitlab.qolq.dik.DIKInteractionManager;
import com.gitlab.qolq.dik.network.DIKPacketListener;
import net.minecraft.advancement.criterion.Criteria;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.network.MessageType;
import net.minecraft.network.packet.s2c.play.BlockUpdateS2CPacket;
import net.minecraft.server.network.ServerPlayNetworkHandler;
import net.minecraft.server.network.ServerPlayerEntity;
import net.minecraft.server.world.ServerWorld;
import net.minecraft.text.TranslatableText;
import net.minecraft.util.ActionResult;
import net.minecraft.util.Formatting;
import net.minecraft.util.Hand;
import net.minecraft.util.Util;
import net.minecraft.util.hit.BlockHitResult;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Direction;
import net.minecraft.util.math.Vec3d;
import net.minecraft.world.World;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.Unique;

@Mixin(ServerPlayNetworkHandler.class)
@SuppressWarnings("unused")
public final class ServerPlayNetworkHandlerMixin implements DIKPacketListener
{
    @Shadow
    public ServerPlayerEntity player;

    @Shadow
    private Vec3d requestedTeleportPos;

    @Shadow
    private static boolean canPlace(ServerPlayerEntity player, ItemStack stack) {return false;}

    @Override
    public void dik$useBlock(BlockHitResult target, Hand hand)
    {
        interactWithBlock(target, hand, ((DIKInteractionManager)player.interactionManager)::dik$useBlock);
    }

    @Override
    public void dik$useItemOnBlock(BlockHitResult target, Hand hand)
    {
        interactWithBlock(target, hand, ((DIKInteractionManager)player.interactionManager)::dik$useItemOnBlock);
    }

    @Unique
    private void interactWithBlock(BlockHitResult target, Hand hand, BlockInteraction interaction)
    {
        ServerWorld world = player.getServerWorld();
        BlockPos position = target.getBlockPos();
        Direction direction = target.getSide();

        player.updateLastActionTime();
        int maxHeight = player.world.getTopY();

        if (position.getY() >= maxHeight)
        {
            player.sendMessage(new TranslatableText("build.tooHigh", maxHeight - 1).formatted(Formatting.RED),
                               MessageType.GAME_INFO, Util.NIL_UUID);
        }
        else if (requestedTeleportPos == null &&
                 player.squaredDistanceTo(position.getX() + 0.5d, position.getY() + 0.5d, position.getZ() + 0.5d) <
                 64.0d && world.canPlayerModifyAt(player, position))
        {
            ActionResult result = interaction.apply(target, world, player, hand);

            if (direction == Direction.UP && !result.isAccepted() && position.getY() >= maxHeight - 1 &&
                canPlace(player, player.getStackInHand(hand)))
            {
                player.sendMessage(new TranslatableText("build.tooHigh", maxHeight - 1).formatted(Formatting.RED),
                                   MessageType.GAME_INFO, Util.NIL_UUID);
            }
            else if (result.shouldSwingHand())
            {
                player.swingHand(hand, true);
            }
        }

        player.networkHandler.sendPacket(new BlockUpdateS2CPacket(world, position));
        player.networkHandler.sendPacket(new BlockUpdateS2CPacket(world, position.offset(direction)));
    }

    @Unique
    private interface BlockInteraction
    {
        ActionResult apply(BlockHitResult target, World world, PlayerEntity player, Hand hand);
    }

    @Override
    public void dik$useEntityAt(Entity target, Hand hand, boolean playerSneaking, Vec3d position)
    {
        interactWithEntity(
            target, hand, playerSneaking,
            ()->((DIKInteractionManager)player.interactionManager).dik$useEntityAt(target, player, hand, position)
        );
    }

    @Override
    public void dik$useEntity(Entity target, Hand hand, boolean playerSneaking)
    {
        interactWithEntity(
            target, hand, playerSneaking,
            ()->((DIKInteractionManager)player.interactionManager).dik$useEntity(target, player, hand)
        );
    }

    @Override
    public void dik$useItemOnEntity(Entity target, Hand hand, boolean playerSneaking)
    {
        interactWithEntity(
            target, hand, playerSneaking,
            ()->((DIKInteractionManager)player.interactionManager).dik$useItemOnEntity(target, player, hand)
        );
    }

    @Unique
    private void interactWithEntity(Entity target, Hand hand, boolean playerSneaking,
                                    Supplier<ActionResult> interaction)
    {
        player.updateLastActionTime();
        player.setSneaking(playerSneaking);

        if (target != null && player.squaredDistanceTo(target) < 36.0d)
        {
            ItemStack stackCopy = player.getStackInHand(hand).copy();
            ActionResult result = interaction.get();

            if (result.isAccepted())
            {
                Criteria.PLAYER_INTERACTED_WITH_ENTITY.test(player, stackCopy, target);
                if (result.shouldSwingHand())
                    player.swingHand(hand, true);
            }
        }
    }
}
