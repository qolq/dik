package com.gitlab.qolq.dik.mixin.client;

import com.gitlab.qolq.dik.DIKInteractionManager;
import com.gitlab.qolq.dik.network.Networking;
import com.gitlab.qolq.dik.network.UseBlockC2SPacket;
import com.gitlab.qolq.dik.network.UseEntityAtC2SPacket;
import com.gitlab.qolq.dik.network.UseEntityC2SPacket;
import com.gitlab.qolq.dik.network.UseItemOnBlockC2SPacket;
import com.gitlab.qolq.dik.network.UseItemOnEntityC2SPacket;
import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;
import net.fabricmc.fabric.api.event.player.UseBlockCallback;
import net.fabricmc.fabric.api.event.player.UseEntityCallback;
import net.minecraft.client.network.ClientPlayerInteractionManager;
import net.minecraft.entity.Entity;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.item.ItemUsageContext;
import net.minecraft.util.ActionResult;
import net.minecraft.util.Hand;
import net.minecraft.util.hit.BlockHitResult;
import net.minecraft.util.hit.EntityHitResult;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Vec3d;
import net.minecraft.world.GameMode;
import net.minecraft.world.World;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;

@Environment(EnvType.CLIENT)
@Mixin(ClientPlayerInteractionManager.class)
@SuppressWarnings("unused")
public abstract class ClientPlayerInteractionManagerMixin implements DIKInteractionManager
{
    @Shadow
    private GameMode gameMode;

    @Shadow
    public abstract void syncSelectedSlot();

    @Override
    public ActionResult dik$useBlock(BlockHitResult target, World world, PlayerEntity player, Hand hand)
    {
        syncSelectedSlot();
        BlockPos position = target.getBlockPos();

        if (!world.getWorldBorder().contains(position))
            return ActionResult.FAIL;

        // --- fabric-events-interaction-v0 mixin
        ActionResult result = UseBlockCallback.EVENT.invoker().interact(player, world, hand, target);
        if (result != ActionResult.PASS)
        {
            if (result == ActionResult.SUCCESS)
                Networking.sendToServer(new UseBlockC2SPacket(target, hand));
            return result;
        }
        // ---

        if (gameMode == GameMode.SPECTATOR)
        {
            Networking.sendToServer(new UseBlockC2SPacket(target, hand));
            return ActionResult.SUCCESS;
        }
        else
        {
            result = world.getBlockState(position).onUse(world, player, hand, target);

            if (result.isAccepted())
            {
                Networking.sendToServer(new UseBlockC2SPacket(target, hand));
                return result;
            }
        }

        return ActionResult.PASS;
    }

    @Override
    public ActionResult dik$useItemOnBlock(BlockHitResult target, World world, PlayerEntity player, Hand hand)
    {
        syncSelectedSlot();
        BlockPos position = target.getBlockPos();

        if (!world.getWorldBorder().contains(position))
            return ActionResult.FAIL;

        // --- fabric-events-interaction-v0 mixin
        ActionResult result = UseBlockCallback.EVENT.invoker().interact(player, world, hand, target);
        if (result != ActionResult.PASS)
        {
            if (result == ActionResult.SUCCESS)
                Networking.sendToServer(new UseBlockC2SPacket(target, hand));
            return result;
        }
        // ---

        if (gameMode == GameMode.SPECTATOR)
        {
            Networking.sendToServer(new UseItemOnBlockC2SPacket(target, hand));
            return ActionResult.SUCCESS;
        }

        ItemStack stack = player.getStackInHand(hand);
        Networking.sendToServer(new UseItemOnBlockC2SPacket(target, hand));

        if (stack.isEmpty() || player.getItemCooldownManager().isCoolingDown(stack.getItem()))
        {
            return ActionResult.PASS;
        }
        else if (gameMode.isCreative())
        {
            int i = stack.getCount();
            result = stack.useOnBlock(new ItemUsageContext(player, hand, target));
            stack.setCount(i);
            return result;
        }
        else
        {
            return stack.useOnBlock(new ItemUsageContext(player, hand, target));
        }
    }

    @Override
    public ActionResult dik$useEntityAt(Entity target, PlayerEntity player, Hand hand, Vec3d position)
    {
        syncSelectedSlot();

        // --- fabric-events-interaction-v0 mixin
        ActionResult result = UseEntityCallback.EVENT.invoker().interact(
            player, player.getEntityWorld(), hand, target,
            new EntityHitResult(target, position.add(target.getX(), target.getY(), target.getZ()))
        );
        if (result != ActionResult.PASS)
        {
            if (result == ActionResult.SUCCESS)
                Networking.sendToServer(new UseEntityAtC2SPacket(target, hand, player.isSneaking(), position));
            return result;
        }
        // ---

        Networking.sendToServer(new UseEntityAtC2SPacket(target, hand, player.isSneaking(), position));
        return gameMode == GameMode.SPECTATOR ? ActionResult.PASS : target.interactAt(player, position, hand);
    }

    @Override
    public ActionResult dik$useEntity(Entity target, PlayerEntity player, Hand hand)
    {
        syncSelectedSlot();
        Networking.sendToServer(new UseEntityC2SPacket(target, hand, player.isSneaking()));

        if (gameMode == GameMode.SPECTATOR)
            return ActionResult.PASS;

        ItemStack stack = player.getStackInHand(hand);
        int count = stack.getCount();
        ActionResult result = target.interact(player, hand);

        if (result.isAccepted())
        {
            if (player.getAbilities().creativeMode && stack == player.getStackInHand(hand) && stack.getCount() < count)
                stack.setCount(count);
            return result;
        }

        return ActionResult.PASS;
    }

    @Override
    public ActionResult dik$useItemOnEntity(Entity target, PlayerEntity player, Hand hand)
    {
        syncSelectedSlot();
        Networking.sendToServer(new UseItemOnEntityC2SPacket(target, hand, player.isSneaking()));

        if (gameMode == GameMode.SPECTATOR)
            return ActionResult.PASS;

        ItemStack stack = player.getStackInHand(hand);
        ItemStack stackCopy = stack.copy();

        if (!stack.isEmpty() && target instanceof LivingEntity)
        {
            if (player.getAbilities().creativeMode)
                stack = stackCopy;

            ActionResult result = stack.useOnEntity(player, (LivingEntity)target, hand);

            if (result.isAccepted())
            {
                if (stack.isEmpty() && !player.getAbilities().creativeMode)
                    player.setStackInHand(hand, ItemStack.EMPTY);
                return result;
            }
        }

        return ActionResult.PASS;
    }
}
