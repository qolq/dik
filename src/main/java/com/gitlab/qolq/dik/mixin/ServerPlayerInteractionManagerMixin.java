package com.gitlab.qolq.dik.mixin;

import com.gitlab.qolq.dik.DIKInteractionManager;
import net.fabricmc.fabric.api.event.player.UseBlockCallback;
import net.fabricmc.fabric.api.event.player.UseEntityCallback;
import net.minecraft.advancement.criterion.Criteria;
import net.minecraft.block.BlockState;
import net.minecraft.entity.Entity;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.item.ItemUsageContext;
import net.minecraft.screen.NamedScreenHandlerFactory;
import net.minecraft.server.network.ServerPlayerEntity;
import net.minecraft.server.network.ServerPlayerInteractionManager;
import net.minecraft.util.ActionResult;
import net.minecraft.util.Hand;
import net.minecraft.util.hit.BlockHitResult;
import net.minecraft.util.hit.EntityHitResult;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Vec3d;
import net.minecraft.world.GameMode;
import net.minecraft.world.World;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;

@Mixin(ServerPlayerInteractionManager.class)
@SuppressWarnings("unused")
public abstract class ServerPlayerInteractionManagerMixin implements DIKInteractionManager
{
    @Shadow
    private GameMode gameMode;

    @Shadow
    public abstract boolean isCreative();

    @Override
    public ActionResult dik$useBlock(BlockHitResult target, World world, PlayerEntity player, Hand hand)
    {
        // --- fabric-events-interaction-v0 mixin
        ActionResult result = UseBlockCallback.EVENT.invoker().interact(player, world, hand, target);
        if (result != ActionResult.PASS)
            return result;
        // ---

        BlockPos position = target.getBlockPos();
        BlockState blockState = world.getBlockState(position);

        if (gameMode == GameMode.SPECTATOR)
        {
            NamedScreenHandlerFactory factory = blockState.createScreenHandlerFactory(world, position);

            if (factory != null)
            {
                player.openHandledScreen(factory);
                return ActionResult.SUCCESS;
            }
            else
            {
                return ActionResult.PASS;
            }
        }
        else
        {
            ItemStack stackCopy = player.getStackInHand(hand).copy();
            result = blockState.onUse(world, player, hand, target);

            if (result.isAccepted())
            {
                Criteria.ITEM_USED_ON_BLOCK.test((ServerPlayerEntity)player, position, stackCopy);
                return result;
            }

            return ActionResult.PASS;
        }
    }

    @Override
    public ActionResult dik$useItemOnBlock(BlockHitResult target, World world, PlayerEntity player, Hand hand)
    {
        // --- fabric-events-interaction-v0 mixin
        ActionResult result = UseBlockCallback.EVENT.invoker().interact(player, world, hand, target);
        if (result != ActionResult.PASS)
            return result;
        // ---

        if (gameMode == GameMode.SPECTATOR)
            return ActionResult.PASS;

        ItemStack stack = player.getStackInHand(hand);

        if (stack.isEmpty() || player.getItemCooldownManager().isCoolingDown(stack.getItem()))
        {
            return ActionResult.PASS;
        }
        else
        {
            ItemStack stackCopy = stack.copy();

            if (isCreative())
            {
                int i = stack.getCount();
                result = stack.useOnBlock(new ItemUsageContext(player, hand, target));
                stack.setCount(i);
            }
            else
            {
                result = stack.useOnBlock(new ItemUsageContext(player, hand, target));
            }

            if (result.isAccepted())
                Criteria.ITEM_USED_ON_BLOCK.test((ServerPlayerEntity)player, target.getBlockPos(), stackCopy);

            return result;
        }
    }

    @Override
    public ActionResult dik$useEntityAt(Entity target, PlayerEntity player, Hand hand, Vec3d position)
    {
        // --- fabric-events-interaction-v0 mixin
        ActionResult result = UseEntityCallback.EVENT.invoker().interact(
            player, player.getEntityWorld(), hand, target,
            new EntityHitResult(target, position.add(target.getX(), target.getY(), target.getZ()))
        );
        if (result != ActionResult.PASS)
            return result;
        // ---

        return target.interactAt(player, position, hand);
    }

    @Override
    public ActionResult dik$useEntity(Entity target, PlayerEntity player, Hand hand)
    {
        if (player.isSpectator())
        {
            if (target instanceof NamedScreenHandlerFactory)
                player.openHandledScreen((NamedScreenHandlerFactory)target);
            return ActionResult.PASS;
        }

        ItemStack stack = player.getStackInHand(hand);
        int count = stack.getCount();
        ActionResult result = target.interact(player, hand);

        if (result.isAccepted())
        {
            if (player.getAbilities().creativeMode && stack == player.getStackInHand(hand) && stack.getCount() < count)
                stack.setCount(count);
            return result;
        }

        return ActionResult.PASS;
    }

    @Override
    public ActionResult dik$useItemOnEntity(Entity target, PlayerEntity player, Hand hand)
    {
        if (player.isSpectator())
            return ActionResult.PASS;

        ItemStack stack = player.getStackInHand(hand);
        ItemStack stackCopy = stack.copy();

        if (!stack.isEmpty() && target instanceof LivingEntity)
        {
            if (player.getAbilities().creativeMode)
                stack = stackCopy;

            ActionResult result = stack.useOnEntity(player, (LivingEntity)target, hand);

            if (result.isAccepted())
            {
                if (stack.isEmpty() && !player.getAbilities().creativeMode)
                    player.setStackInHand(hand, ItemStack.EMPTY);
                return result;
            }
        }

        return ActionResult.PASS;
    }
}
