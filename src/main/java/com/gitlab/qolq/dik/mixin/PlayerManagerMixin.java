package com.gitlab.qolq.dik.mixin;

import com.gitlab.qolq.dik.network.EnableModS2CPacket;
import com.gitlab.qolq.dik.network.Networking;
import net.minecraft.network.ClientConnection;
import net.minecraft.server.PlayerManager;
import net.minecraft.server.network.ServerPlayerEntity;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.Slice;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

@Mixin(PlayerManager.class)
@SuppressWarnings("unused")
public final class PlayerManagerMixin
{
    @Inject(
        method = "onPlayerConnect(Lnet/minecraft/network/ClientConnection;" +
                 "Lnet/minecraft/server/network/ServerPlayerEntity;)V",
        slice = @Slice(
            from = @At(value = "NEW", target = "net/minecraft/network/packet/s2c/play/SynchronizeTagsS2CPacket"),
            to = @At(value = "INVOKE", target = "Lnet/minecraft/stat/ServerStatHandler;updateStatSet()V")
        ),
        at = @At(
            value = "INVOKE",
            target = "Lnet/minecraft/server/PlayerManager;sendCommandTree" +
                     "(Lnet/minecraft/server/network/ServerPlayerEntity;)V",
            shift = At.Shift.BY,
            by = -2
        )
    )
    private void sendEnableModPacket(ClientConnection connection, ServerPlayerEntity player, CallbackInfo info)
    {
        Networking.sendToClient(new EnableModS2CPacket(), player);
    }
}
