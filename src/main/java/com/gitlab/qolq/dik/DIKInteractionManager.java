package com.gitlab.qolq.dik;

import net.minecraft.entity.Entity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.util.ActionResult;
import net.minecraft.util.Hand;
import net.minecraft.util.hit.BlockHitResult;
import net.minecraft.util.math.Vec3d;
import net.minecraft.world.World;

public interface DIKInteractionManager
{
    ActionResult dik$useBlock(BlockHitResult target, World world, PlayerEntity player, Hand hand);

    ActionResult dik$useItemOnBlock(BlockHitResult target, World world, PlayerEntity player, Hand hand);

    ActionResult dik$useEntityAt(Entity target, PlayerEntity player, Hand hand, Vec3d position);

    ActionResult dik$useEntity(Entity target, PlayerEntity player, Hand hand);

    ActionResult dik$useItemOnEntity(Entity target, PlayerEntity player, Hand hand);
}
