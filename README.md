## ![Icon](src/main/resources/icon.png) Dedicated Interaction Key

_A Minecraft mod which creates a new key binding for interacting with
blocks/mobs so right-clicking doesn't do too many things_

- Homepage: <https://www.gitlab.com/qolq/dik/wikis>
- Sources: <https://www.gitlab.com/qolq/dik>
- Issue Tracker:
  - <incoming+qolq-dik-28999549-issue-@incoming.gitlab.com>
  - <https://www.gitlab.com/qolq/dik/issues>

---

Copyright (c) 2020 qolq

The software included in Dedicated Interaction Key is free to
redistribute and/or modify under the terms of the GNU Lesser General
Public License as published by the Free Software Foundation, either
version 3 of the License, or (at your option) any later version.

The artworks included in Dedicated Interaction Key are free to
redistribute and/or modify under the terms of the Creative Commons
Attribution-ShareAlike 4.0 International License.

Dedicated Interaction Key is distributed in the hope that it will be
useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
Lesser General Public License for more details.

You should have received copies of the licenses along with Dedicated
Interaction Key. If not, see <https://www.gnu.org/licenses/> and
<https://www.creativecommons.org/licenses/by-sa/4.0/>
