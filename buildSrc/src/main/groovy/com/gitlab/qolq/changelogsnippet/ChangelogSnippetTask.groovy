package com.gitlab.qolq.changelogsnippet

import org.gradle.api.DefaultTask
import org.gradle.api.tasks.Input
import org.gradle.api.tasks.InputFile
import org.gradle.api.tasks.OutputFile
import org.gradle.api.tasks.TaskAction

class ChangelogSnippetTask extends DefaultTask {
    @InputFile
    File inputFile = project.file('CHANGELOG.md')

    @OutputFile
    File outputFile = project.file("${project.buildDir}/changelogSnippet/CHANGELOG.md")

    @Input
    String headerPattern = /^##\s*\[(?:Unreleased|\d+(?:\.\d+){1,}.*)\]/

    @Input
    int depth = 1

    @TaskAction
    def run() {
        def snippet = ''
        def pattern = ~headerPattern
        def currentDepth = 0
        def scanner = new Scanner(inputFile)

        while (scanner.hasNextLine()) {
            def line = scanner.nextLine()
            if (line =~ pattern && ++currentDepth > depth)
                break
            if (currentDepth > 0)
                snippet += line + '\n'
        }

        scanner.close()
        outputFile.write(snippet)
    }
}
